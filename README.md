# _api-alumnos_

Esta API se utiliza para registrar estudiantes y calificaciones.

## Pre-requisitos

A continuación se muestran las herramientas necesarias, para poder ejecutar esta API localmente.

|HERRAMIENTA      |VERSIÓN       |DESCRIPCIÓN                  |LINK DE DESCARGA                                 |
|---------------- |--------------|-----------------------------|------------------------------------------------ |
|Spring Tool Suite|4.7.0.RELEASE |IDE de desarrollo            |[SpringToolSuite](https://spring.io/tools/)      |
|Java             |1.8.0         |Lenguaje de programación     |[Java](https://www.java.com/es/download/)        |
|Git              |2.25.0        |Control de versiones         |[Git](https://git-scm.com/downloads)             |
|Postgress        |12.0.0        |Sistema de Gestion de Bd     |[Postgress](https://www.postgresql.org/download) |

*
## Inicio rapido 

* Clonar el proyecto

  Abrir un terminal donde más convenga y pegar el siguiente código:
   git clone https://Alejandra1994@bitbucket.org/Alejandra1994/alumnos.git


  

* Importar proyecto en Spring Tool Suite
Una vez abierto el IDE, hacer lo siguiente:

-> File
   -> Import
      -> Existing maven Project 
         -> Buscar directorio de descarga y abrir proyecto
            -> Finish


* Ejecutando proyecto

-> Click derecho en el proyecto
   -> Run As 
      -> Spring Boot App 

*
## Despliegue

* Consideraciones para el despliegue

Se debe iniciar el processo de postgres
Usuario:postgres
Password:postgres


Una vez considerado lo anterior, abrimos la siguiente dirección:

http://localhost:8082/swagger-ui.html#/

Nota: El puerto puede cambiarse en el archivo de propiedades


*
## Construido con 

* [SpringFramework](https://spring.io/) - Framework usado para el desarrollo del API
* [Gradle](https://gradle.org/) - Manejador de dependencias
* [Git](https://git-scm.com/) - Software de control de versiones

*
## Versionado

Para el versionado y alojamiento del código se utiliza [BitBucket](https://bitbucket.org/)
*
## Autores

* *Alejandra Ortíz Vázquez* - [GitHub](https://github.com/)